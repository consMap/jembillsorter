﻿using Bytescout.Spreadsheet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Net.Http;
using System.Text;

namespace Application
{
    public class Program
    {
        private static string LogPath { get; set; }
        private static string ExcelPath { get; set; }
        private static string BillPath { get; set; }
        private static string ApiBaseUri { get; set; }

        static void Main(string[] args)
        {
            Console.WriteLine("Your Bills are being generated..");
            try
            {
                Initialize();
                Generate();
            }
            catch (Exception e)
            {
                Console.WriteLine("\n\nSomething bad has happened\n");
                Console.WriteLine($"Message : {e.Message}");
                using (var file = File.CreateText($"{LogPath}/Error.json"))
                {
                    file.Write(JsonConvert.SerializeObject(e));
                    file.Close();
                }
                Console.WriteLine("\nFailure.... Press Enter to exit");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("\n\nSucess.... Press Enter to exit");
            Console.ReadKey();
        }

        static void Initialize()
        {
            ApiBaseUri = ConfigurationManager.AppSettings["ApiBaseUri"];
            ExcelPath = ConfigurationManager.AppSettings["ExcelPath"];
            BillPath = ConfigurationManager.AppSettings["BillPath"] + $"/{DateTime.Now.ToString("MMMM_yyyy")}";
            LogPath = ConfigurationManager.AppSettings["LogPath"];
            CheckAndCreateFolder(ExcelPath);
            CheckAndCreateFolder(BillPath);
            CheckAndCreateFolder(LogPath);
        }

        static void CheckAndCreateFolder(string path)
        {
            if (!Directory.Exists($"{path}"))
            {
                Directory.CreateDirectory($"{path}");
            }
        }

        static void Api(DailyBill body, string billName)
        {
            using (var client = new HttpClient
            {
                BaseAddress = new Uri(ApiBaseUri)
            })
            {
                var response = client.PostAsync("Print/A6", new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json")).Result;
                if (response.IsSuccessStatusCode)
                {
                    var json = response.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<PrintResult>(json);
                    var imgString = result.ImageData;
                    var bmp = new Bitmap(new MemoryStream(Convert.FromBase64String(imgString)));
                    Print(bmp, billName);
                    var file = File.Open($"{BillPath}/{billName}.png", FileMode.Create);
                    bmp.Save(file, ImageFormat.Png);
                    file.Close();
                }
            }
        }

        static void Print(Image bill, string billName)
        {
            var setting = new PrinterSettings
            {
                PrintFileName = billName
            };
            var printer = new PrintDocument()
            {
                OriginAtMargins = true,
                DefaultPageSettings = new PageSettings()
                {
                    Margins = new Margins()
                    {
                        Left = 0,
                        Top = 0,
                        Right = 0,
                        Bottom = 0
                    },
                    PaperSize = new PaperSize() { RawKind = (int)PaperKind.A6 },
                    PrinterResolution = Convert.ToBoolean(ConfigurationManager.AppSettings["HighRes"]) ?
                    new PrinterResolution { Kind = PrinterResolutionKind.High } : new PrinterResolution { Kind = PrinterResolutionKind.Low },
                    Color = Convert.ToBoolean(ConfigurationManager.AppSettings["IsColor"]),
                },
                DocumentName = billName
            };
            printer.PrintPage += Printer_PrintPage;
            void Printer_PrintPage(object sender, PrintPageEventArgs e)
            {
                int width = Convert.ToInt32(printer.DefaultPageSettings.PrintableArea.Width);
                int height = Convert.ToInt32(printer.DefaultPageSettings.PrintableArea.Height);
                if (ConfigurationManager.AppSettings["PaperX"] != string.Empty && ConfigurationManager.AppSettings["PaperY"] != string.Empty)
                {
                    width = Convert.ToInt32(ConfigurationManager.AppSettings["PaperX"]);
                    height = Convert.ToInt32(ConfigurationManager.AppSettings["PaperY"]);
                }
                if (ConfigurationManager.AppSettings["PrintType"].Equals("Scaled", StringComparison.InvariantCultureIgnoreCase))
                {
                    e.Graphics.DrawImage(bill, setting.DefaultPageSettings.HardMarginX, setting.DefaultPageSettings.HardMarginY, width, height);
                }
                else if (ConfigurationManager.AppSettings["PrintType"].Equals("UnScaled", StringComparison.InvariantCultureIgnoreCase))
                {
                    e.Graphics.DrawImageUnscaled(bill, 0, 0, width, height);
                }
            }
            printer.Print();
        }

        static void Generate()
        {
            Console.WriteLine("Opening the excel document..");
            var document = new Spreadsheet();
            document.LoadFromFile($"{ExcelPath}/JemBill.xlsx");
            var dict = new Dictionary<string, List<ItemL>>();
            Console.Write("Reading Data");
            foreach (Worksheet workSheet in document.Worksheets)
            {
                Console.Write(" .");
                var supervisor = workSheet.Name;
                if (supervisor == "Sheet2")
                {
                    continue;
                }
                var rows = workSheet.UsedRangeRowMax;
                for (int i = 1; i < rows; i++)
                {
                    var date = workSheet.Cell(i, 0).ValueAsString;
                    var dcNum = workSheet.Cell(i, 1).ValueAsString;
                    var siteName = workSheet.Cell(i, 2).ValueAsString;
                    var itemName = workSheet.Cell(i, 3).ValueAsString;
                    var unit = workSheet.Cell(i, 4).ValueAsString;
                    var qty = workSheet.Cell(i, 5).ValueAsString;
                    var rate = workSheet.Cell(i, 6).ValueAsString;
                    var amt = workSheet.Cell(i, 7).ValueAsString;
                    if (string.IsNullOrEmpty(date))
                    {
                        break;
                    }
                    if (!string.IsNullOrEmpty(dcNum))
                    {
                        if (!dict.ContainsKey(siteName))
                        {
                            dict.Add(siteName, new List<ItemL>
                            {
                                new ItemL
                                {
                                    Date = Convert.ToDateTime(date),
                                    Amount = amt,
                                    Rate = rate,
                                    Qty = qty,
                                    ItemName = itemName,
                                    DcNumber = dcNum
                                }
                            });
                        }
                        else
                        {
                            dict[siteName].Add(new ItemL
                            {
                                Date = Convert.ToDateTime(date),
                                Amount = amt,
                                Rate = rate,
                                Qty = qty + unit,
                                ItemName = itemName,
                                DcNumber = dcNum
                            });
                        }
                    }
                }
            }

            foreach (var bill in dict)
            {
                var billQ = new Dictionary<string, List<ItemL>>();
                var qCount = 1;
                for (int c = 0; c < bill.Value.Count; c++)
                {
                    if (c != 0 && c % 25 == 0)
                    {
                        qCount++;
                    }
                    var qName = bill.Key + "-" + qCount.ToString();
                    if (billQ.ContainsKey(qName))
                    {
                        billQ[qName].Add(bill.Value[c]);
                    }
                    else
                    {
                        billQ.Add(qName, new List<ItemL>
                        {
                            bill.Value[c]
                        });
                    }
                }
                Console.Write("\nPrinting");
                foreach (var qbill in billQ)
                {
                    var dailyBill = new DailyBill
                    {
                        BillNumber = "123",
                        ClientName = "JEM Constructions - " + qbill.Key.Split('-')[0],
                        Date = DateTime.Now.ToString("MMMM-yyyy"),
                        Items = new List<Item>()
                    };
                    var billAmount = 0f;
                    var dcNumbers = new List<string>();
                    var dcString = "Ind : ";
                    foreach (var item in qbill.Value)
                    {
                        billAmount += Convert.ToSingle(item.Amount);
                        if (!dcNumbers.Contains(item.DcNumber))
                        {
                            if (dcNumbers.Count != 0)
                            {
                                dcString += ",";
                            }
                            dcNumbers.Add(item.DcNumber);
                            dcString += $"{item.DcNumber}";
                        }
                        dailyBill.Items.Add(new Item
                        {
                            Amount = item.Amount,
                            Name = item.ItemName,
                            Quantity = item.Qty,
                            Rate = item.Rate
                        });
                    }
                    dailyBill.Amount = billAmount.ToString();
                    dailyBill.Details = dcString;
                    Console.Write(" .");
                    Api(dailyBill, qbill.Key);
                }
            }
        }
    }
    public class ItemL
    {
        public DateTime Date { get; set; }
        public string DcNumber { get; set; }
        public string ItemName { get; set; }
        public string Qty { get; set; }
        public string Rate { get; set; }
        public string Amount { get; set; }
    }

    public class DailyBill
    {
        public string BillNumber { get; set; }
        public string ClientName { get; set; }
        public string ClientNumber { get; set; }
        public string Details { get; set; }
        public string Date { get; set; }
        public List<Item> Items { get; set; }
        public string Discount { get; set; }
        public string Amount { get; set; }
        public string TotalCgst { get; set; }
        public string TotalSgst { get; set; }
    }

    public class Item
    {
        public string Name { get; set; }
        public string Rate { get; set; }
        public string Quantity { get; set; }
        public Gst Gst { get; set; }
        public string Amount { get; set; }
    }

    public class Gst
    {
        public string Hsn { get; set; }
        public Component Cgst { get; set; }
        public Component Sgst { get; set; }
    }

    public class Component
    {
        public string Percentage { get; set; }
        public string Amount { get; set; }
    }

    public class PrintResult
    {
        public Paper Paper { get; set; }
        public string ImageFormat { get; set; }
        public string ImageData { get; set; }
    }

    public class Paper
    {
        public string Type { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string PixelPerMil { get; set; }
    }
}
